#!/usr/bin/perl
use strict;
use warnings;

my $sourceDb = "imdb";
my $destDb = "imdb_dw";
my $username = "postgres";
my $host = "hammerco.no-ip.org";

my @source = `psql -U $username $sourceDb -h $host -c 'select count(*) from title.crew'`;
my $sourceRows = $source[2];
print "number of records from source table title.crew: ", $sourceRows;

my @dest = `psql -U $username $destDb -h $host -c 'select count(*) from imdb_dw.crew'`;
my $destRows = $dest[2];
print "number of records to dest table imdb_dw.crew: ", $destRows;

if ($sourceRows == $destRows) {
  print "Test status: PASS\n";
} else {
  print "Test status: FAIL\n";
}
 