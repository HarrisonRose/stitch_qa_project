#!/usr/bin/perl
use strict;
use warnings;

my $sourceDb = "imdb";
my $destDb = "imdb_dw";
my $username = "postgres";
my $host = "hammerco.no-ip.org";

# TODO - Hook in PostgreSQL - Perl Interface
# https://www.tutorialspoint.com/postgresql/postgresql_perl.htm
my $tableQuery = 'SELECT table_schema, table_name, table_type FROM information_schema.tables where table_schema not in (\'information_schema\', \'pg_catalog\') and table_name != \'_sdc_rejected\'';

# Get number of tables in source database
my @source = `psql -U $username $sourceDb -h $host -c "$tableQuery"`;
my $soureTableCount = -1;
if ($source[$#source -1] =~ m/(\d+) /) {
    $soureTableCount = $1;
}

print "number of Tables from source database: ", $soureTableCount, "\n";

# Get number of tables in destination database
my @dest = `psql -U $username $destDb -h $host -c "$tableQuery"`;
my $destTableCount = -1;
if ($dest[$#dest - 1] =~ m/(\d+) /) {
    $destTableCount = $1;
}

print "number of Tables from destination database: ", $destTableCount, "\n";

# Compare and determine results
if ($soureTableCount == $destTableCount) {
  print "Test status: PASS\n";
} else {
  print "Expected ", $soureTableCount, " tables.\nActually ",
    $destTableCount, " tables.\n Test status: FAIL\n";
}
 