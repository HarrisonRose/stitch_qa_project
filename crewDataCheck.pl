#!/usr/bin/perl
use strict;
use warnings;

# TODO - don't rely on formatting of previous tests
# Determine if previous results pass.
my @results = `perl crewRowCount.pl`;
if (index($results[$#results],"PASS") != -1) {
    print "Prerequisites passed, continuing...\n";
} else {
    exit;
}

# Extract rows from previous test results
my $rows;
if ($results[1] =~ m/(\d+)/) {
    $rows = $1
}

my $sourceDb = "imdb";
my $destDb = "imdb_dw";
my $username = "postgres";
my $host = "hammerco.no-ip.org";
my $sampleSize = 5;
my $counter = 0;
my $offset = 0;
my $limit = 1000;

print "Checking ", $sampleSize, " queries of ", $limit, " rows for data integrity.\n";

############################
# TODO - improve performance {index source table (and see if it follows to dest table)}
#   possibly get a bigger batch of data than a single row.
#   constantly logging in might take some time too.
# TODO - parameritize db info using 
#   SELECT table_schema, table_name, table_type FROM information_schema.tables where table_schema not in ('information_schema', 'pg_catalog');
#   and other info tables.
############################

# Check random rows for data integrity
while ($counter++ < $sampleSize) {
    $offset = rand() * $rows;
    my @sourceData = `psql -U $username $sourceDb -h $host -c 'select tconst, directors, writers from title.crew order by tconst limit $limit offset $offset'`;
    my @destData = `psql -U $username $destDb -h $host -c 'select tconst, directors, writers from imdb_dw.crew order by tconst limit $limit offset $offset'`;

    if (@sourceData ~~ @destData) {
        print '.';
    } else {
        print "\nmismatch at offset $offset: FAIL\n";
        exit;
    }
}

print "\nAll rows match: PASS\n";

