I believe the most important part of the product to test is data integrity.  In that spirit I have created a couple of sample tests that connect to the source and destination databases and pull statistics to ensure proper transfer of data.  

I started checking that the proper number of tables exist in the destination.  
Next I tested that the number of rows in a Table matched as expected.
Finally I tested that a random sampling of data matched as expected.

I created the tests in perl (which I'm pretty rusty with).  Please note that I wouldn't consider this a "framework" and that these tests need a lot of work to be made into a framework. They need to be tuned for performance, need to be parameterized for expandability, need to be updated with DBI instead of just using bash commands.

I created a ~/.pgpass which is required as a prerequisite to running the tests. The tests run on my localhost where the databases reside. I updated the code with a dynamic dns host to run remotely.  Also, I have installed postgres and configured it to allow your four IPs to have connectivity to my database.  However, I am going through a double NAT with port forwarding to my personal Mac which is not always on and connected to the internet. I apologize if you don't have acces if/when you attempt to run them.

You can find the code @ https://bitbucket.org/HarrisonRose/stitch_qa_project/overview which I have made a public repository. I also have a private repository which I used to learn C# which I can make public if you wanted to see a code sample of a larger project.

During this testing I noticed something unexpected with the product and my first test currently reports a fail. I setup my free account to use the postgres integration into a postgres destination.  My source database had two schemas "title" and "name" both with a table named "basics" (and one more table).  When reviewing the data warehouse the schema wasn't included in the destination table name which caused a collision in table names in the warehouse.  The table had an end state of having the columns from both name.basics and title.basics but only the data from one of the two tables.  Therefore one table and the data from it didn't actually get to the data warehouse.  Your billing rows reported number of rows appeared to indicate that the rows were moved over so I'm guessing the data may have been overwritten by the last table load.  You should note that I created the database from scratch and for the sake of speed didn't use the suggested updated_at columns so therefore have data loading with Full Table Replication.

Please let me know if you have any questions about the project or would like any additional information.

Thanks,

Harrison

